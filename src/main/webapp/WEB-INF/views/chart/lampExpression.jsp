<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}
#wraper{
	overflow: scroll;
	overflow-x:hidden;
	height : 80%;
}
.mainView{
	overflow: hidden;
	overflow-x:hidden;
}
@keyframes redBlink {
    from {color: red;}
    to {color: rgb(167, 1, 1);}
}

/* The element to apply the animation to */
.redBlink{
    animation-name: redBlink;
    animation-duration: 0.1s;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    will-change: transform;
    transform: translateZ(0);
}
@keyframes yellowBlink {
    from {color: yellow;}
    to {color: rgb(144, 146, 3);}
}

/* The element to apply the animation to */
.yellowBlink{
    animation-name: yellowBlink;
    animation-duration: 0.1s;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    will-change: transform, opacity;
    transform: translateZ(0);
}
@keyframes greenBlink {
    from {color: green;}
    to {color: gray;}
}

/* The element to apply the animation to */
.greenBlink{
    animation-name: greenBlink;
    animation-duration: 0.1s;
    animation-iteration-count: infinite;
    animation-direction: alternate;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
    will-change: transform, opacity;
    transform: translateZ(0);
}

.green {
    color: #339933;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}
.yellow {
    color: #ffff00;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}
.red {
    color: #ff0000;
    text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black;
}
</style> 
<script type="text/javascript">
	
	var shopId=2;
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function timer_eu(){
		$("#time").html(getToday_eu());
		handle = requestAnimationFrame(timer_eu);
	}
	
	
	$(function(){
		//getAppList();
		setDate();
		createNav("config_nav", 2);
		setEl();
		time();
		
		var lang = window.localStorage.getItem("lang");
		
		if(lang=="de") {
			cancelAnimationFrame(handle)
			$("#time").html(getToday_eu());
			handle = requestAnimationFrame(timer_eu);
		}
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		getLampExpresion();
	});
	
	function useModify(val){
		console.log($(val).parent().parent())
		$(val).parent().parent().find('td').find('input:text').each (function() {
			if($(val).prop("checked")){
				$(this).prop('disabled', false);
			}else{
				$(this).prop('disabled', true);
			}
		})
		
		$(val).parent().parent().find('td').find('button').each (function() {
			if($(val).prop("checked")){
				$(this).prop('disabled', false);
			}else{
				$(this).prop('disabled', true);
			}
			
		})

	}
	
	function checkRegExp(id){
	    var open = /^[(]$/;
	    var close = /^[)]$/;
	    var pattern = /^[ADEFGKRXY][0-9]+[.][0-9]$/;
	    var notPattern = /^[!][ADEFGKRXY][0-9]+[.][0-9]$/;
	    var or = /^[|]$/;
	    var and = /^[&]$/;
	    var not = /^[!]$/;

	    var stringData = $("#"+id).val();
	    
	    /* console.log(stringData); */
	    
	    var strArray = stringData.split(' ');
	    /* console.log("배열의 길이는 : " + strArray.length);*/
	 
	    var matching = false;
	    for(var i=0;i<strArray.length;i++){
	       /*  console.log("배열의 각 항목 : " + strArray[i]); */
	        if(strArray[i].trim().match(open)!=null || 
	        strArray[i].trim().match(close)!=null || 
	        strArray[i].trim().match(and)!=null || 
	        strArray[i].trim().match(not)!=null || 
	        strArray[i].trim().match(or)!=null ){
	            matching = true;
	           /*  console.log("strArray[i].match(open)" + strArray[i].match(open))
	            console.log("strArray[i].match(close)" + strArray[i].match(close))
	            console.log(" strArray[i].match(or)" +  strArray[i].match(or)) */
	        }else if(  strArray[i].match(pattern)!=null || 
	        strArray[i].match(notPattern)!=null ){
	            matching = true;
	           /*  console.log("strArray[i].match(pattern)" + strArray[i].match(pattern))
	            console.log("strArray[i].match(notPattern) " + strArray[i].match(notPattern)) */
	            strArray[i] = 1;
	        }else{
	            matching = false;
	           /*  console.log("매칭 안됨") */
	            alert("올바른 수식이 아닙니다.");
	            return false;
	        }
	    }



	    var expression="";
	    if(matching){
	        /* console.log("모두 매칭됩니다.") */
	        for(var i=0;i<strArray.length;i++){
	            expression+=strArray[i]
	        }
	        console.log(expression)
	        try{
	            console.log(eval(expression))
	            alert("올바른 수식 입니다.")
	            var r = confirm("${save} 하시겠습니까?");
				if (r == true) {
					
					console.log(id)
					
					var extractId = id.replace(/[^0-9]/g,"");
					var extractExpression = id.replace(/[^a-zA-Z]+/,"");
					
					console.log("id : "+ extractId +", ExpressionName : " + extractExpression+", Expression :"+  stringData);
					stringData = stringData.replace(/&/gi, "%26");
					var url = "${ctxPath}/setLampExpression.do"
					var param = "dvcId=" + extractId + 
						"&expressionName=" + extractExpression +"Expression"+
						"&expression=" + stringData;
					console.log(url+param)
					
					
					$.ajax({
						url : url,
						data: param,
						dataType : "text",
						type : "post",
						success : function(data){
							if(data=="success"){
								alert("성공적으로 ${save} 되었습니다.")
								location.reload();
							}
						}
					})
						
				} else {
					
				}
							
	        }catch(exception){
	            alert("올바른 수식이 아닙니다.")
	        }
	        
	    }else{
	        alert("매칭되지 않는 문자열 입니다. 제대로 기입하여 주세요.")
	    }

	}
	
	function getLampExpresion(){
		var url = "${ctxPath}/getLampExpression.do"
		var param = "shopId=" + shopId
		console.log("shopId : " + shopId);
		console.log(url+param)
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(lamp){

				var obj = lamp.dvcId
				console.log(lamp)
				
				$(".tmpTable").find("tr:gt(0)").remove();
				$(obj).each(function(i, data){
					
					var redExpression = data.redExpression
					if(redExpression!=null){
						var redExpression = redExpression.replace(/\uFFFD/g, ' ')
					}
					var redBlinkExpression = data.redBlinkExpression
					if(data.redBlinkExpression!=null){
						redBlinkExpression = redBlinkExpression.replace(/\uFFFD/g, ' ')
					}else{
						redBlinkExpression = '';
					}
					var yellowExpression = data.yellowExpression
					if(yellowExpression!=null){
						yellowExpression = yellowExpression.replace(/\uFFFD/g, ' ')
					}
					var yellowBlinkExpression = data.yellowBlinkExpression
					if(yellowBlinkExpression!=null){
						yellowBlinkExpression = yellowBlinkExpression.replace(/\uFFFD/g, ' ')
					}else{
						yellowBlinkExpression = '';
					}
					var greenExpression = data.greenExpression
					if(greenExpression!=null){
						greenExpression = greenExpression.replace(/\uFFFD/g, ' ')
					}
					var greenBlinkExpression = data.greenBlinkExpression
					if(greenBlinkExpression!=null){
						greenBlinkExpression = greenBlinkExpression.replace(/\uFFFD/g, ' ')
					}else{
						greenBlinkExpression = '';
					}
					
					var tr = "<tr>" + 
							 "<td class='name cell'>" + data.name + "</td>" +
							 "<td class='cell'><input type='checkBox' id='"+data.dvcId+"' onclick='useModify(this)'></td>" +
							 "<td class='cell'><input type='text' id='red"+data.dvcId+"' disabled value='" + redExpression + "'><button disabled onclick=checkRegExp('red"+data.dvcId+"')>${confirm} / ${save}</button></td>" +
							 "<td class='cell'><input type='text' id='redBlink"+data.dvcId+"' disabled value='" + redBlinkExpression + "'><button disabled onclick=checkRegExp('redBlink"+data.dvcId+"')>${confirm} / ${save}</button></td>" +
							 "<td class='cell'><input type='text' id='yellow"+data.dvcId+"' disabled value='" + yellowExpression + "'><button disabled onclick=checkRegExp('yellow"+data.dvcId+"')>${confirm} / ${save}</button></td>" +
							 "<td class='cell'><input type='text' id='yellowBlink"+data.dvcId+"' disabled value='" + yellowBlinkExpression + "'><button disabled onclick=checkRegExp('yellowBlink"+data.dvcId+"')>${confirm} / ${save}</button></td>" +
							 "<td class='cell'><input type='text' id='green"+data.dvcId+"' disabled value='" + greenExpression + "'><button disabled onclick=checkRegExp('green"+data.dvcId+"')>${confirm} / ${save}</button></td>" +
							 "<td class='cell'><input type='text' id='greenBlink"+data.dvcId+"' disabled value='" + greenBlinkExpression + "'><button disabled onclick=checkRegExp('greenBlink"+data.dvcId+"')>${confirm} / ${save}</button></td>" +
							 "</tr>"
									
					$(".tmpTable > tbody:last").append(tr);
				})
				
				$(".cell").css({
					"height" : getElSize(200),
					"padding" : getElSize(20)
				})
				
				$("input[type='text']").css({
					"font-size" : getElSize(60),
					"width" : "100%"
				})
				
				$("button").css({
					"font-size" : getElSize(60),
					"margin-top" : getElSize(50),
					"width" : getElSize(350)
				})
				
				$("input[type='checkBox']").css({
					"width" : "50%",
					"height" : "50%"
				})
				
				$(".name").css({
					"font-size" : getElSize(60)
				})
				
				$("#nameHeader").css({
					"background" : "#222222",
					"font-size" : getElSize(60)
				})
				
				$("#wraper").scroll(function() {
					//$('.name').css('transform', 'translateX('+ $(this).scrollLeft() +'px)');
					$(".thead").css('transform', 'translateY('+ $(this).scrollTop() +'px)');
				});
				
				/* $("input[type='text']").focus(function(){
					$(this).css({
						"position" : "fixed",
						"width" : "25%"
					})
					
					$(this).blur(function(){
						$(this).css({
							"position" : "relative",
							"width" : "100%"
						})
					})
				}) */
			}
		})
		
	}
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"text-align" : "center",
			"color" : "white",
			"font-size" : getElSize(35),
			"padding" : getElSize(20),
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#wraper").css({
			"width" : "100%"
		})
		
		$(".tmpTable").css({
			"width" : "100%"			
		})
		
		$("#wraper").css({
			"height" : getElSize(1750)
		})
		
	}
		
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<spring:message  code="chk_del"></spring:message>
		<div><spring:message  code="check"></spring:message></div>
		<div><spring:message  code="cancel"></spring:message></div>
	</div>
	
	<iframe id="app_store_iframe"  style="display: none"></iframe>	
	
	
	<div id="time"></div>
	<div id="title_right"></div>
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/config_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/sky_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
				<td class="mainView" rowspan="10" id="svg_td" style="vertical-align: top;">
					<div id="wraper">
						<table style="color: white; text-align: center; border-collapse: collapse;" class="tmpTable" border="1">
							<tr style="font-weight: bolder;background-color: rgb(34,34,34)" class="thead">
								<th id="nameHeader" class="name" style="width:10%"><spring:message  code="device"></spring:message></th>
								<th style="width:5%"><spring:message  code="modify"></spring:message></th>
								<th class="titleHead red"><spring:message  code="red"></spring:message></th>
								<th class="titleHead redBlink"><spring:message  code="redBlink"></spring:message></th>
								<th class="titleHead yellow"><spring:message  code="yellow"></spring:message></th>
								<th class="titleHead yellowBlink"><spring:message  code="yellowBlink"></spring:message></th>
								<th class="titleHead green"><spring:message  code="green"></spring:message></th>
								<th class="titleHead greenBlink"><spring:message  code="greenBlink"></spring:message></th>
							</tr>	
						</table>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span" ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'> </span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	
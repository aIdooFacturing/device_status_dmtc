
package com.unomic.dulink.chart.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ChartVo {
	String jig;
	String mcTy;
	String group;
	String ncTy;
	String WC;

	// common
	String ty;
	String fileLocation;
	String url;
	String fileName;
	String appId;
	String appName;
	String categoryId;
	Integer shopId;
	String dvcId;
	String id;
	String pwd;
	String name;
	String msg;
	String rgb;
	
	private String userid;
	private int count;
	private int red;
	private int redBlink;
	private int yellow;
	private int yellowBlink;
	private int green;
	private int greenBlink;
	private String address;
	private String addressValue;

	String redExpression;
	String redBlinkExpression;
	String yellowExpression;
	String yellowBlinkExpression;
	String greenExpression;
	String greenBlinkExpression;
	
	String expressionName;
	String expression;
	String idx;
	
	String time;
}
